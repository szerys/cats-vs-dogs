# README #

### Projekt programistyczny ###

Zakładam w tym projekcie stworzenie oraz wytrenowanie konwolucyjnej sieci neuronowej na podstawie zebranych danych, zdolnej do rozpoznawania zwierząt, a dokładniej różnic między kotem a psem.

### Użyte technologie ###

* TenorFlow
* Keras
* konwolucyjne sieci neuronowe
* Język skryptowy Python
* Tensorboard


# Raport 1 #

Przygotowanie środowiska pod nauczenie maszynowe, pobranie potrzebnych bibliotek, oraz sterowników do kart. Zebranie danych potrzebnych do trenowania sieci neuronowej, następnie obrobienie ich pod tą sieć. 

# Raport 2 #

Rozpoczęcie trenowania danych, problem z obsługą procesora oraz karty graficznej przez sieć neuronową. Nieudana próby trenowania, które zajmowały po parę godzin.
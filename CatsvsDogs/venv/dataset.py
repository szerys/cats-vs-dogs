import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
import random

PHOTOS = r"C:\Users\Piotr\PycharmProjects\Dataset\PetImages"

PETS = ["Dog", "Cat"]

IMG_SIZE = 70

training_data = []


def traindata():
    for category in PETS:

        path = os.path.join(PHOTOS, category)
        class_num = PETS.index(category)  # dog 1=cat

        for image in os.listdir(path):
            try:
                image_array = cv2.imread(os.path.join(path, image), cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(image_array, (IMG_SIZE, IMG_SIZE))  # resize
                training_data.append([new_array, class_num])
            except Exception as e:  
                pass


traindata()

print(len(training_data))

random.shuffle(training_data)

X = []
y = []

for features,label in training_data:
    X.append(features)
    y.append(label)

X = np.array(X).reshape(-1, IMG_SIZE, IMG_SIZE, 1)

np.save('X.features.npy',X)
np.save('y.features.npy',y)






